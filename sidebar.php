<div class="col-md-3">
<?php
/**
 * @file    : Displaying left menu
 * @author  : Rizwan Iliyas <web4mybiz@gmail.com>
 * @license : GPLv2+
 */
    wp_nav_menu(['theme_location' => 'left-menu']);
?>
</div>