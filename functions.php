<?php
declare(strict_types=1);
/**
 * @file    : This file loads all the required functions for theme to work
 * @author  : Rizwan Iliyas <web4mybiz@gmail.com>
 * @license : GPLv2+
 */


function inpsydeScripts()
{
    wp_enqueue_style('inpsyde-basic-style', get_stylesheet_uri());
    wp_enqueue_style('inpsyde-basic-blog', get_template_directory_uri().'/css/blog.css', '', '1.1', 'all');
}
add_action('wp_enqueue_scripts', 'inpsydeScripts');

// Adding featured image support to theme.
add_theme_support('post-thumbnails', [ 'post', 'page' ]);


/**
 * Registering theme menu
 **/
function registerThemeMenu()
{
    register_nav_menu('left-menu', __('Left Menu', 'inpsydebasic'));
}

add_action('init', 'registerThemeMenu');


/**
 * Adding read more link to excerpt
 **/
function addReadMore($more)
{
    return ' <a href="'.get_the_permalink().'" rel="nofollow" class="read-more">[read more]</a>';
}
add_filter('excerpt_more', 'addReadMore');
