<?php
declare(strict_types=1);
/**
 * @file    : This template loads all posts details and attachments
 * @author  : Rizwan Iliyas <web4mybiz@gmail.com>
 * @license : GPLv2+
 */

get_header();
?>

    <div class="row mb-2">
        <?php get_sidebar(); ?>
        <div class="col-md-9">
        <?php
        if (have_posts() !== true) :
            return esc_html_e('Sorry, no posts found.', 'inpsydebasic');
        endif;
        if (have_posts() === true) :
            while (have_posts() === true) :
                the_post();
                ?>
                <div class="card-flex flex-md-row mb-4">
                    <div class="card-body d-flex flex-column align-items-start">
                        <h1 class="mb-0">
                            <a class="text-dark" href="#"><?php the_title('<h1>', '</h1>'); ?></a>
                        </h1>
                        <p class="card-text mb-auto"><?php the_content(); ?></p>
                    </div>
                </div>

                <?php
            endwhile;
        endif;
        ?>
        </div>
    </div>
<?php get_footer();

