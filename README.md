# README #
### Ins Basic theme ###
Requires at least: 5.0
Tested up to: 5.3.2
Requires PHP: 7.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This theme is built for testing purpose for Inpsyde.
This is a basic theme is built on bootstrap for blog posts and event management integration.

### Installation ###
Theme can be downloaded through bitbucket repository 
<pre><code>$ git pull https://bitbucket.org/web4mybiz/ins-basic
</code></pre>

or download it manually and upload it in the wordpress themes directory or through wordpress backend.

Create posts and add featured image to view the home page filled with posts just like the demo. 



### About ###
This theme displays all blog posts in the home and also works with integrated event management plugin. Even pages displays all the event information filled up in the backend.


### Changelog ###

* Basic theme file structure created 
* Bootstrap installed via composer.js
* php_codesniffer installed via composer
* inpsyde/php-coding-standards installed via composer
* Integrated inpsyde/php-coding-standards with PhpStorm
* Home page layout index.php created with posts dummy data and tested.
* single.php created and tested with data.
* Added event management plugin dependant pages.
* Event archive page created and set to list all events.
* Even detail page created and invoke the respective Event class to load event data.
* screenshot.png added to theme root.


