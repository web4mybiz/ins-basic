
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">

    <title><?php bloginfo('name'); ?></title>
    <!-- Bootstrap core CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:700,900" rel="stylesheet">
    <?php wp_head(); ?>
</head>
<body>
<div class="container">
    <header class="blog-header py-3">
        <div class="row flex-nowrap justify-content-between align-items-center mb-md-5">
            <div class="col-md-3  col-sm-12 text-center">
                <?php
                if (function_exists('the_inpsyde_logo') === __return_false()) :
                    echo '<a class="blog-header-logo text-dark" href="'.get_home_url().'"><img src="'.get_template_directory_uri().'/img/Mountain-Conqueror.png" width="200"></a>';
                endif;
                ?>
                <?php
                if (function_exists('the_inpsyde_logo') === true) :
                    the_inpsyde_logo();
                endif;
                ?>
            </div>
            <div class="col-9 justify-content-end text-center d-none d-md-block">
                <h1 class="display-4 font-italic"><?php bloginfo('name'); ?></h1>
                <p class="lead my-3"><?php bloginfo('description'); ?></p>

            </div>
        </div>
    </header>