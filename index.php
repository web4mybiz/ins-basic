<?php
declare(strict_types=1);
/**
 * @file    : This template loads the blog posts in the home page
 * @author  : Rizwan Iliyas <web4mybiz@gmail.com>
 * @license : GPLv2+
 */

get_header();
?>

    <div class="row mb-2">
        <?php get_sidebar(); ?>
        <div class="col-md-9">
        <?php
        if (have_posts() !== true) :
            return esc_html_e('Sorry, no posts found.', 'inpsydebasic');
        endif;
        if (have_posts() === true) :
            while (have_posts() === true) :
                the_post();
                ?>
            <div class="post-wrapper card-flex flex-md-row mb-4">
                <div class="card-body d-flex flex-column align-items-start">
                    <h3 class="mb-0">
                        <a class="text-dark" href="<?php the_permalink();?>"><?php the_title('<h2>', '</h2>'); ?></a>
                    </h3>
                    <p class="card-text mb-auto"><?php the_excerpt(); ?></p>
                    <div class="mb-1">
                    <?php
                        the_date('jS \of F Y');
                        echo " > ";
                        the_author();
                        echo " > ";
                        the_category();
                    ?>
                    </div>
                </div>
                <?php if (has_post_thumbnail() === true) : ?>
                    <?php the_post_thumbnail(['300', '300'], [ 'class' => 'card-img-right d-none d-md-block rounded-circle' ]);?>
                <?php endif; ?>
            </div>

                <?php
            endwhile;
        endif;
        ?>
        </div>
    </div>
<?php get_footer();

