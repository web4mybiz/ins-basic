<?php
declare(strict_types=1);
/**
 * @file    : This template loads all event details
 * @author  : Rizwan Iliyas <web4mybiz@gmail.com>
 * @license : GPLv2+
 */

get_header();
$event = Event::fromPost(get_post());
?>

    <div class="row mb-2">
        <?php get_sidebar(); ?>
        <div class="col-md-9">
        <?php
        if (have_posts() !== true) :
            return esc_html_e('Sorry, no events found.', 'inpsydebasic');
        endif;
        if (have_posts() === true) :
            while (have_posts() === true) :
                the_post();
                ?>
                <div class="flex-md-row mb-4">
                    <div class="card-body d-flex flex-column align-items-start">
                        <h1 class="mb-0">
                            <a class="text-dark" href="#"><?php the_title('<h1>', '</h1>'); ?></a>
                        </h1>
                        <div class="mb-1 text-muted"><?php the_date(); ?></div>
                        <?php if (has_post_thumbnail() === true) : ?>
                            <?php the_post_thumbnail(['200', '250'], [ 'class' => 'card-img-right d-none d-md-block' ]);?>
                        <?php endif; ?>
                        <p class="card-text mb-auto"><?php the_content(); ?></p>
                    </div>
                </div>
                <dl class="row">

                        <dt class="col-sm-3">Date of Event:</dt>
                        <dd class="col-sm-9"><?php echo $event->startDate()." - ".$event->endDate(); ?></dd>
                        <dt class="col-sm-3">Time:</dt>
                        <dd class="col-sm-9">8:00 - 16:00</dd>

                        <dt class="col-sm-3">Location:</dt>
                        <dd class="col-sm-9">
                            <dl class="row">
                                <dt class="col-sm-4">
                                    <address>
                                        <?php
                                            var $eventLocation= $event->location();
                                            echo $eventLocation->name().",".$eventLocation->street()."<br>";
                                            echo $eventLocation->postalCode().",".$eventLocation->city()."<br>";
                                            echo $eventLocation->country();
                                        ?>
                                    </address></dt>
                                <dd class="col-sm-8">
                                    <strong>Contact person:</strong>
                                    <div>
                                    <?php
                                        var $eventContact= $event->contactPerson();
                                        echo $eventContact->firstName()." ".$eventContact->lastName()."<br>";
                                        echo $eventLocation->position()."<br>";
                                        echo $eventLocation->telephone()."<br>";
                                        echo $eventLocation->email().;
                                    ?>
                                    </div>
                                </dd>
                            </dl>
                        </dd>
                </dl>
                <dl class="row">
                        <dt class="col-sm-3">Subscriber:</dt>
                        <dd class="col-sm-9"><?php echo $event->subscribedMin()." - ".$event->subscribedMax(); ?></dd>

                        <dt class="col-sm-3">Price:</dt>
                        <dd class="col-sm-9">110 €</dd>

                        <dt class="col-sm-3">Included in price:</dt>
                        <dd class="col-sm-9"><?php echo implode(",", $event->includedInPrice()); ?></dd>

                        <dt class="col-sm-3">Registration end:</dt>
                        <dd class="col-sm-9"><?php echo $event->registrationEnd(); ?></dd>
                </dl>

                <?php
            endwhile;
        endif;
        ?>
        </div>
    </div>
<?php get_footer();

