<?php
declare(strict_types=1);
/**
 * @file    : This template loads all the events
 * @author  : Rizwan Iliyas <web4mybiz@gmail.com>
 * @license : GPLv2+
 */

get_header();
?>

    <div class="row mb-2">
        <?php get_sidebar(); ?>
        <div class="col-md-9">
            <div class="row">
            <?php
            // The query to get events.
            $theEvents = new WP_Query(['post_type' => 'events']);
            if ($theEvents->have_posts() !== true) :
                return esc_html_e('Sorry, no events found.', 'inpsydebasic');
            endif;
            if ($theEvents->have_posts() === true) :
                while ($theEvents->have_posts() === true) :
                       $theEvents->the_post();
                    ?>
                    <div class="post-wrapper card-flex flex-md-row mb-4 col-md-6">
                        <div class="card-body d-flex flex-column align-items-start">
                            <div class="mb-1">
                                <?php
                                the_date('jS \of F Y');
                                echo " > Germany";
                                ?>
                            </div>
                            <h3 class="mb-0">
                                <a class="text-dark" href="<?php echo get_the_permalink();?>"><?php the_title('<h2>', '</h2>'); ?></a>
                            </h3>
                            <p class="card-text mb-auto"><?php the_excerpt(); ?></p>
                        </div>
                    </div>

                    <?php
                endwhile;
            endif;
            ?>
            </div>
        </div>
    </div>
<?php get_footer();



